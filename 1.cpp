// 1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>
#include <vector>

class Vector
{
public:
    Vector()
    {
        x = 0;
        y = 0;
        z = 0;
        Info = new std::string("hello");
    }

    Vector(float num) = delete;
    

	Vector(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        Info = new std::string("hello");
    }

    Vector(const Vector& other)
    {
        std::cout << "\n Copy constructor \n";
        x = other.x;
        y = other.y;
        z = other.z;
        Info = new std::string("hello");
    }

    ~Vector()
    {
        //std::cout << "destructor \n";
        if (Info)
        {
            delete Info;
        }
        
    }

    Vector& operator=(Vector& other)
    {
        std::cout << "operator = ";
        x = other.x;
        y = other.y;
        z = other.z;

        if (other.Info)
        {
            if (Info) delete Info;
            Info = new std::string(*(other.Info));
        }
        

        return (*this);
    }


    operator float()
    {
        return sqrt(x * x + y * y + z * z);
    }

    friend Vector operator+(const Vector& a, const Vector& b);

    friend Vector operator*(const Vector& a, const int b);

    friend Vector operator-(const Vector& a, const Vector& b);

    friend std::ostream& operator<<(std::ostream& out, const Vector);

    friend std::istream& operator>> (std::istream& in, Vector& v);

    friend bool operator>(const Vector& a, const Vector& b);

    float operator[](int index)
    {
        switch (index)
        {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            std::cout << "index error";
            return 0;
            break;
        }
    }

private:
    float x;
    float y;
    float z;

    std::string* Info;
};


bool operator>(const Vector& a, const Vector& b)
{
    return false;
}

Vector operator+(const Vector& a, const Vector& b)
{
   return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector operator*(const Vector& a, const int b)
{
    return Vector(a.x * b, a.y * b, a.z * b);
}

Vector operator-(const Vector& a, const Vector& b)
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

std::ostream& operator<<(std::ostream& out, const Vector v)
{
    out <<' '<< v.x <<' '<< v.y <<' '<< v.z;
    return out;
}

std::istream& operator>>(std::istream& in,  Vector& v)
{
    in >> v.x >> v.y >> v.z;
    
    return in;
}


class Matrix
{
    
private:
    std::string* Name;
    int** data;
    int row, col;

public:
    Matrix(int row, int col) : row(row), col(col) {
        data = new int* [row];
        for (int i = 0; i < row; ++i) {
            data[i] = new int[col];
            for (int j = 0; j < col; ++j) {
                data[i][j] = 0;
            }
        }
        Name = new std::string("Matrix");
    }
    ~Matrix() {
        for (int i = 0; i < row; ++i) {
            delete[] data[i];
        }
        delete[] data;
        if (Name)
        {
            delete Name;
        }
    }
    void set(int row, int col, int value) {
        data[row][col] = value;
    }

    int get(int row, int col) {
        return data[row][col];
    }



    friend std::ostream& operator<< (std::ostream& output, const Matrix& matrix) // перезагрузка оператора <<
    {
        for (int i = 0; i < matrix.row; i++)
        {
            for (int j = 0; j < matrix.col; j++)
            {
                output << matrix.data[i][j];
            }
            std::cout << std::endl;
        }

        output << std::endl; 

        return output; 
    }       



    /*void print() {

        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                std::cout << data[i][j] << " ";
              
            }
            std::cout << "\n";
        }
    }*/
    Matrix(const Matrix& other)
    {
        data = other.data;
        row = other.row;
        col = other.col;
        std::cout << "\n Copy constructor \n";
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                data[i][j] = other.data[i][j];
            }
        }


        Name = new std::string("Matr");
    }

    Matrix& operator=(Matrix& other)
    {
       
        std::cout << "operator = ";
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                data[i][j] = other.data[i][j];
            }
        }

        if (other.Name)
        {
            if (Name) delete Name;
            Name = new std::string(*(other.Name));
        }

        return (*this);
    }
    
};


int main()
{
    Matrix Matr1(2, 2);
    Matr1.set(1, 1, 3);
    Matr1.set(0, 0, 1);
    std::cout << Matr1;
    //Matr1.print();

    Matrix Matr2(2,2);
    Matr2 = Matr1;
    std::cout << std::endl;
    std::cout << Matr2;
    //Matr2.print();


    /*Vector v1(1, 1, 1);
    Vector v2(2, 2, 2);
    Vector v3;
    std::cout << v2;
    v3 = v2 = v1;
    
    std::cout << v3;*/







    //int b = 2;
    //v3 = v1 + v2;
    //std::cout << v3 << '\n';
    /*v3 = v1 * b;
    std::cout << v3 <<'\n';
    v3 = v1 - v2;
    std::cout << v3 << '\n';

    Vector v4{};
    std::cin >> v4;

    std::cout << v4 << '\n';*/

    //std::cout << v1 + v2 << '\n';
    
}
